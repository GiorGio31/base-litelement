import {LitElement, html} from 'lit-element';

class PersonaFooter extends LitElement {

    render(){
        return html`
            <h5>
                @PersonaApp 2020
            </h5>
        `;
    }

}

customElements.define('persona-footer', PersonaFooter);