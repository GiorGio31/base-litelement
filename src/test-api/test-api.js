import {LitElement, html} from 'lit-element';

class TestApi extends LitElement {
    
    static get properties() {
        return{
            movies: {Type: Array}
        };
    }

    constructor(){
        super();
        this.movies = [];
        this.getMovieData();
    }

    render(){
        return html`
        ${this.movies.map(
            movie => html`<div>La pelicula ${movie.title} fue dirigida por el director ${movie.director}</div>`
        )}
        `;
    }

    getMovieData(){
        console.log("getmoviedata");
        console.log("Obteniendo datos de peliculas...");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200) {
                console.log("Petición completada");
                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                this.movies = APIResponse.results;
                
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films/");
        //El body de la petición se manda en el send. GET no tiene body, pero POST tendría.
        xhr.send();
        console.log("Fin de getmoviedata");
    }
}

customElements.define('test-api', TestApi);