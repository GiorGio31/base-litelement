import {LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js';


class PersonaMain extends LitElement {

    static get properties(){
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
            yearsFilter: {type: Number}
        };
    }

    constructor(){
        super();
        this.people = [];

        this.showPersonForm = false;
        this.yearsFilter = 0;
    }

    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
        crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map( person => this.filtros(person) ? html`<persona-ficha-listado 
                        fname="${person.name}" 
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deleteperson}"
                        @info-person="${this.infoPerson}"
                        ></persona-ficha-listado>` : null)}
                </div>
            </div>
            <div class ="row">
                <persona-form id="personForm" class="d-none" @persona-form-store="${this.personFormStore}" @persona-form-close="${this.personFormClose}"></persona-form>
            </div>
            <persona-main-dm  @carga-datos-people="${this.cargaDatos}"></persona-main-dm>
        `;
    }

    cargaDatos(e){
        console.log("cargadatos en persona-main");
        this.people = e.detail.people;

    }

    //funcion para incluir filtros por los cuales se muestra o no un elemento de people
    filtros(person){
        if(person.yearsInCompany <= this.yearsFilter)
            return true;
        else
            return false;

    }

    infoPerson(e){
        console.log("infoPerson en persona-main");
        console.log("Se pide mas info de la persona "+e.detail.name);

        let chosenPerson = this.people.filter(person => person.name === e.detail.name);
        console.log(chosenPerson[0]);

        this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
        
    }

    deleteperson(e){
        console.log("delete person en persona-main");

        this.people = this.people.filter(person => person.name != e.detail.name);
    }

    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("showPersonForm")){
            console.log("Ha cambiado la propiedad showpersonform");
            if(this.showPersonForm === true){
                this.showPersonFormData();
            }
            else{
                this.showPersonList();
            }
        }
        if(changedProperties.has("people")){
            console.log("Ha cambiado la propiedad people XXXXXXXXXXXX");
            console.log(this.people);

            this.dispatchEvent(new CustomEvent("updated-people", {
                detail: {
                    people: this.people
                }
            }))
        }
        if(changedProperties.has("yearsFilter")){
            console.log("Ha cambiado yearsFilter. Se van a mostrar las persona con antiguedad menor a "+ this.yearsFilter);

        }
    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");

        console.log(e.detail);
        if(e.detail.editingPerson){
            console.log("Se va a actualizar la persona con nombre "+e.detail.person.name);

            this.people = this.people.map( person => person.name === e.detail.person.name ? person = e.detail.person : person);
            /* Forma altenativa pero que no desencadena Updated
            let indexOfPerson = this.people.findIndex(person => person.name === e.detail.person.name);
            if(indexOfPerson >= 0){
                this.people[indexOfPerson] = e.detail.person;
                
            }*/
        }else{
            console.log("Se va a almacenar una persona nueva");
            let personsaInsertar = e.detail.person;
            personsaInsertar.photo = {
                src: "./img/goomba.png",
                alt: "nuevo"
            }

            /* Forma altenativa pero que no desencadena Updated
            this.people.push(personsaInsertar);
            */
            this.people = [...this.people, personsaInsertar];
        }

        this.showPersonForm = false;
    }

    personFormClose(){
        console.log("personFormclose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");  
    }
    showPersonList(){
        console.log("showPersonList");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

}

customElements.define('persona-main', PersonaMain);