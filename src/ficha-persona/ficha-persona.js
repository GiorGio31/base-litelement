import {LitElement, html} from 'lit-element';

class FichaPersona extends LitElement {

    static get properties(){
        return {
            name: {type:String},
            yearsInCompany: {type:Number},
            personInfo: {type:String},
            photo: {type:Object}
        };
    }
    
    calculaPersonInfo(){
        if(this.yearsInCompany >= 7)
            this.personInfo = "Lead";
        else if(this.yearsInCompany >= 5)
            this.personInfo = "Senior";
        else if(this.yearsInCompany >= 3)
            this.personInfo = "Team";
        else 
            this.personInfo = "Junior";
    }

    constructor(){
        super();

        this.name= "Pepe Perez";
        this.yearsInCompany=12;
        this.photo= {
            src: "./img/persona.jpg",
            alt: "foto persona"
        }
        this.calculaPersonInfo();

    }

    

    updated(changedProperties){
        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad ["+propName+ "] cambia valor, anterior era ["+oldValue+"]");
        });

        if(changedProperties.has("name")){
            console.log("Propiedad name cambia valor, antes era "+ changedProperties.get("name") + " nuevo es " +this.name);
        }

        if(changedProperties.has("yearsInCompany")){
            console.log("Propiedad personInfo cambia valor, antes era "+ changedProperties.get("personInfo") + " nuevo es " +this.personInfo);
            this.updatePersonInfo();
        }
    }

    render(){
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la Epresa</label>
                <input type="text" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <label>Categoria</label>
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
                <img src="${this.photo.src}" alt="${this.photo.alt}" width="200" height="400"></img>
            </div>
        `;
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando propiedad nombre con el valor "+e.target.value);
        this.name = e.target.value;
    }
    updateYearsInCompany(e){
        console.log("updateYears");
        console.log("Actualizando propiedad Years con el valor "+e.target.value);
        this.yearsInCompany = e.target.value;
        this.calculaPersonInfo();
    }
    updatePersonInfo(){
        console.log("updatePersonInfo");
        console.log("Years vale "+this.yearsInCompany);
        this.calculaPersonInfo();
    }

}

customElements.define('ficha-persona', FichaPersona);