import {LitElement, html} from 'lit-element';

class PersonaHeader extends LitElement {

    render(){
        return html`
            <h1>
                App Molona
            </h1>
        `;
    }

}

customElements.define('persona-header', PersonaHeader);