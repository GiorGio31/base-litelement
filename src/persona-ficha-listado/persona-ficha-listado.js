import {LitElement, html} from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties(){
        return {
            fname:{type: String},
            yearsInCompany: {type: Number},
            profile: {type: String},
            photo: {type: Object}
        }
    }


    render(){
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
        crossorigin="anonymous">
            <div class="card" h-100>
                <img src="${this.photo.src}" alt="${this.photo.alt}" height="300" width="10" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${this.fname}</h5>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button class="btn btn-danger col-5" @click="${this.deleteperson}"><strong>X</strong></button>
                    <button class="btn btn-info col-5" @click="${this.moreInfo}"><strong>Info</strong></button>
                </div>
                </div>
            </div>
        `;
    }

    deleteperson(){
        console.log("deleteperson en persona-ficha-listado");
        console.log("Se va borrar la persona con el nombre "+this.fname);

        this.dispatchEvent(
            new CustomEvent(
                "delete-person",
                {
                    detail: {
                        name: this.fname
                    }
                }
            )
        );
    }

    moreInfo(e){
        console.log("moreInfo");
        console.log("Se ha pedido información de la persona "+this.fname);

        this.dispatchEvent(
            new CustomEvent(
                "info-person",
                {
                    detail: {
                        name: this.fname
                    }
                }
            )
        );

    }

}

customElements.define('persona-ficha-listado', PersonaFichaListado);