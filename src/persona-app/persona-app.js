import {LitElement, html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

    constructor(){
        super();

    }

    render(){
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
            crossorigin="anonymous">
            <persona-header></persona-header>
            <persona-stats id="personaStats" @updated-stats-people="${this.updatedStatsPeople}"></persona-stats>   
            <div class="row">
                <persona-sidebar id="personaSidebar" class="col-2" @new-person="${this.newPerson}" @updated-yearsFilter="${this.updateYearsFilter}"></persona-sidebar>
                <persona-main id="personaMain" class="col-10" @updated-people="${this.updatedPeople}"></persona-main>
            </div>
            <persona-footer></persona-footer>
        `;
    }

    

    newPerson(e){
        console.log("newPerson en persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updatedPeople(e){
        console.log("captando el evento updatedPeople en persona-app");
        this.shadowRoot.getElementById("personaStats").people = e.detail.people;

    }

    updatedStatsPeople(e){
        console.log("captando el evento updatedStatsPeople en persona-app");
        this.shadowRoot.getElementById("personaSidebar").countPeople = e.detail.peopleStats.countPeople;
        this.shadowRoot.getElementById("personaSidebar").maxYears = e.detail.peopleStats.maxYears;

    }

    updateYearsFilter(e){
        console.log("captando el evento cambio yearsFilter en persona-app valor: "+e.detail.yearsFilter);
        this.shadowRoot.getElementById("personaMain").yearsFilter = e.detail.yearsFilter;

    }

}

customElements.define('persona-app', PersonaApp);