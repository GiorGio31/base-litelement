import {LitElement, html} from 'lit-element';

class PersonaStats extends LitElement {

    static get properties(){
        return{
            people: {type: Array},
            peopleStats: {type: Object},
        }
    }

    constructor(){
        super();
        this.people = []; 
        this.peopleStats = {};
    }

    updated(changedProperties){
        if(changedProperties.has("people")){
            console.log("ha cambiado people y estoy en persona-stats");
            this.calculaPeople();
        }
    }
    calculaPeople(){
        console.log("Calculando el numero de personas y el maxYears");

        let max=0;
        this.peopleStats.countPeople=this.people.length;
        this.people.forEach(person => person.yearsInCompany > max ? max=person.yearsInCompany : null);
        this.peopleStats.maxYears = max;

        console.log("countPeople: "+this.peopleStats.countPeople);
        console.log("maxYears: "+this.peopleStats.maxYears);

        this.dispatchEvent(new CustomEvent("updated-stats-people", {
            detail: {
                peopleStats: this.peopleStats
            }
        }))


    }
}

customElements.define('persona-stats', PersonaStats);