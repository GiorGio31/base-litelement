import {LitElement, html} from 'lit-element';

class PersonaSidebar extends LitElement {
    
    static get properties(){
        return{
            yearsFilter: {type: Number},
            countPeople: {type: Number},
            maxYears: {type: Number}
        }
    }

    constructor(){
        super();
        this.maxYears = 0;
        this.countPeople = 0;
        this.yearsFilter = 0;
    }
    
    render(){
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" 
            crossorigin="anonymous">
            <aside>
                <section>
                    <div class="mt-5">
                        <button class="w-100 btn btn-success" style="font-size:50px" @click="${this.newPerson}"><strong>+</strong></button>
                    </div>
                    <div class="mt-5">
                        Hay <span class="badge badge-pill badge-primary">${this.countPeople}</span> Personas
                    </div>
                    <div class="mt-5">
                        <input type="range" id="rango" class="bg-secondary" value="${this.yearsFilter}" min="0" max="${this.maxYears}" step="1" @input="${this.cambioRange}" />
                        <span class="badge badge-pill badge-secondary">${this.yearsFilter}</span>
                    </div>
                </section>
            </aside>
        `;
    }

    cambioRange(e){
        console.log("cambiando el valor del range");
        this.yearsFilter = e.target.value;
        
        this.dispatchEvent(new CustomEvent("updated-yearsFilter", {
            detail: {
                yearsFilter: this.yearsFilter
            }
        }));
    }

    newPerson(e){
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una persona");

        this.dispatchEvent(new CustomEvent("new-person", {}));
    }

    updated(changedProperties){
        if(changedProperties.has("maxYears")){
            console.log("Ha cambiado maxYears en persona-sidebar valor: "+this.maxYears+" y filtro: "+this.yearsFilter);
            this.yearsFilter = this.maxYears;

            this.dispatchEvent(new CustomEvent("updated-yearsFilter", {
                detail: {
                    yearsFilter: this.yearsFilter
                }
            }));

        }
    }
}

customElements.define('persona-sidebar', PersonaSidebar);