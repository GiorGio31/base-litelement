import {LitElement, html} from 'lit-element';

class ReceptorEvento extends LitElement {
    
    static get properties(){
        return {
            course: {type: String},
            year: {type: String}
        }
    }
    
    render(){
        return html`
            <h3>Receptor evento</h3>
            <h5>Este curso es ${this.course}</h5>
            <h5>y estamos en el año ${this.year}</h5>

        `;
    }
}

customElements.define('receptor-evento', ReceptorEvento);