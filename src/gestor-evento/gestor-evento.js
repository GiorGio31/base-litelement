import {LitElement, html} from 'lit-element';
import "../emisor-evento/emisor-evento.js"
import "../receptor-evento/receptor-evento.js"


class GestorEvento extends LitElement {
    render(){
        return html`
            <h1>Gestor evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent(e){
        console.log("Capturado evento emisor");
        console.log(e.detail);

        this.shadowRoot.getElementById("receiver").course = e.detail.course;
        this.shadowRoot.getElementById("receiver").year = e.detail.year;
        
    }
}

customElements.define('gestor-evento', GestorEvento);