import {LitElement, html} from 'lit-element';

class EmisorEvento extends LitElement {
    render(){
        return html`
            <h3>Emisor evento</h3>
            <button @click="${this.sendEvent}">No Pulsar</button>
        `;
    }

    sendEvent(e){
        console.log("evento enviado");

        this.dispatchEvent(
            new CustomEvent(
                "test-event", 
                {
                    "detail": {
                        course: "TechU",
                        year: 2020
                    }
                }
            )
        );
    }
}

customElements.define('emisor-evento', EmisorEvento);