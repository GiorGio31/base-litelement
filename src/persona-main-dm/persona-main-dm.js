import {LitElement, html} from 'lit-element';


class PersonaMainDM extends LitElement {

    static get properties(){
        return {
            people: {type: Array}
        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Mario",
                yearsInCompany: 5,
                profile: "Fontanero pro",
                photo: {
                    src: "./img/mario.jpg",
                    alt: "fotomario"
                }
            },
            {
                name: "Luigi",
                yearsInCompany: 7,
                profile: "Fontanero pro",
                photo: {
                    src: "./img/luiggi.png",
                    alt: "fotoluiggi"
                }
            },
            {
                name: "Toad",
                yearsInCompany: 2,
                profile: "Seta mágica",
                photo: {
                    src: "./img/toad.png",
                    alt: "fototoad"
                }
            },
            {
                name: "Yoshi",
                yearsInCompany: 6,
                profile: "Dinosaurio verde",
                photo: {
                    src: "./img/yoshi.png",
                    alt: "fotoyoshi"
                }
            },
            {
                name: "Wario",
                yearsInCompany: 9,
                profile: "Fontanero malvado",
                photo: {
                    src: "./img/wario.png",
                    alt: "fotowario"
                }
            }
        ];

    }



    updated(changedProperties){
        console.log("cargando en persona-main-dm");

        if(changedProperties.has("people")){
            console.log("Ha cambiado la propiedad people en DM");

            this.dispatchEvent(new CustomEvent("carga-datos-people", {
                detail: {
                    people: this.people
                }
            }))
        }
    }

    



}

customElements.define('persona-main-dm', PersonaMainDM);